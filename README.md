DEMO INVOICE REGISTER
===================================

# SpringBoot Application

1. H2 DATABASE CONSOLE LINK  (http://localhost:8080/h2-console)
   

# Libraries Included

1. H2Database Runtime
2. Spring Boot Starter Web
3. Spring Boot Starter Data JPA
4. Spring Boot Starter Test
5. Lombok (Optional)
	
