package za.co.bmwdemo.configuration;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import za.co.bmwdemo.model.Invoice;
import za.co.bmwdemo.service.InvoiceService;

@WebMvcTest
class InvoiceControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private InvoiceService invoiceService;
	
//	Invoice invoice1 = new Invoice("Nelson", 15);
	
	String exampleInvoiceJson = "{\"client\": \"DummyClient\",\"vatRate\": 12}";
	@Test
	public void getInvoice() throws Exception {
		Mockito.when(invoiceService.getInvoice(Mockito.anyLong()));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/invoices").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse());
		
		String expected = "{\"id\": 1,\"client\": \"dummy\",\"vatRate\": 15,\"invoiceDate\": null,\"lineItem\": null}";
		
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

}
