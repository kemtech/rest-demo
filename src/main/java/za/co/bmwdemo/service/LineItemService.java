package za.co.bmwdemo.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import za.co.bmwdemo.model.Invoice;
import za.co.bmwdemo.model.LineItem;
import za.co.bmwdemo.repository.InvoiceRepository;
import za.co.bmwdemo.repository.LineItemRepository;

@Service
public class LineItemService {
	
	@Autowired
	private LineItemRepository lineItemRepository;
	
	@Autowired
	private InvoiceRepository invoiceRepository;
	
	public void addLineItem(LineItem lineItem, long invoiceId) {
		Invoice invoice = invoiceRepository.findById(invoiceId).get();
		invoice.setId(invoiceId);
		lineItem.setInvoice(invoice); 
		lineItemRepository.save(lineItem);
	}
	
	public List<LineItem> getLineItems(long invoiceId){
		List<LineItem> lineItems = new ArrayList<LineItem>();
		lineItemRepository.findByInvoiceId(invoiceId).forEach(lineItems::add);
		return lineItems;
	}
	
	public LineItem getLineItem(Long id) {
		return lineItemRepository.findById(id).get();
	}
}
