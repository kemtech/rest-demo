package za.co.bmwdemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import za.co.bmwdemo.model.Invoice;
import za.co.bmwdemo.model.LineItem;
import za.co.bmwdemo.repository.InvoiceRepository;

@Service
public class InvoiceService {
	@Autowired
	private InvoiceRepository invoiceRepository;
		
	
	public void addInvoice(Invoice invoice) {
		for(LineItem lineItem:invoice.getLineItems())
		{
			lineItem.setInvoice(invoice);
		}
		invoiceRepository.save(invoice);
	}
	
	public List<Invoice> getInvoices(){
		List<Invoice> invoices = new ArrayList<Invoice>();
		invoiceRepository.findAll().forEach(invoices::add);
		return invoices;
	}
	
	public Optional<Invoice> getInvoice(Long id) {
		return invoiceRepository.findById(id);
	}
}
