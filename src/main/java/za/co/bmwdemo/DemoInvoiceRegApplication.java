package za.co.bmwdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoInvoiceRegApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoInvoiceRegApplication.class, args);
	}

}
