package za.co.bmwdemo.repository;

import org.springframework.data.repository.CrudRepository;

import za.co.bmwdemo.model.Invoice;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

}
