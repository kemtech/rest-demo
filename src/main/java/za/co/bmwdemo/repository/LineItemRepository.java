package za.co.bmwdemo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import za.co.bmwdemo.model.LineItem;

public interface LineItemRepository extends CrudRepository<LineItem, Long>{
	public List<LineItem> findByInvoiceId(Long invoiceId);
}
