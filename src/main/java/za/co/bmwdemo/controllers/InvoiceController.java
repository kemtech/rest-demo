package za.co.bmwdemo.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import za.co.bmwdemo.model.Invoice;
import za.co.bmwdemo.service.InvoiceService;

@RestController
//@RequestMapping("/invoices")
public class InvoiceController {
	
	@Autowired
	private InvoiceService invoiceService;
		
//	@PostMapping("/invoices")
	@RequestMapping(value="/invoices", method = RequestMethod.POST)
	public void addInvoice(@RequestBody Invoice invoice) {
		invoiceService.addInvoice(invoice);
	}
	
	@RequestMapping("/invoices")
	public List<Invoice> viewAllInvoices(){
		return invoiceService.getInvoices();
	}
	
	@RequestMapping("/invoices/{InvoiceId}")
	public Invoice viewInvoice(@PathVariable("InvoiceId") Long id) {
		return invoiceService.getInvoice(id).get();
	}
	
	
	
}
