package za.co.bmwdemo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;

@Builder
@Entity
@Table(name = "invoice")
public class Invoice implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;
	@Column(name = "client")
	private String client;
	@Column(name = "vatRate")
	private long vatRate;
	@Column(name = "invoiceDate")
	private Date invoiceDate = new Date();
	
	@OneToMany(mappedBy = "invoice", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnoreProperties("invoice")
    private Set<LineItem> lineItems;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public Invoice() {}
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public long getVatRate() {
		return vatRate;
	}

	public void setVatRate(long vatRate) {
		this.vatRate = vatRate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Set<LineItem> getLineItems() {
		return lineItems;
	}
	public void setLineItems(Set<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	public BigDecimal getSubTotal() {
        BigDecimal subTotal = new BigDecimal(0);
        lineItems.forEach(lineItem -> {
            subTotal.add(lineItem.getLineItemTotal());
        });
        return subTotal;
    }

	public BigDecimal getVat() {
		return getSubTotal().multiply(BigDecimal.valueOf(getVatRate()));
	}
	public BigDecimal getTotal() {
		return getSubTotal().add(getVat());
	}
}
